import React from 'react'
import { Button,Menu,Typography,Avatar } from 'antd'
import {Link} from 'react-router-dom'
import {MenuOutlined,FundOutlined,BulbFilled,MoneyCollectFilled,HomeOutlined} from '@ant-design/icons'
import icon from '../assets/img/logo.png'
const Navbar = () => {
  return (
    <div className='nav-container'>
        <div className='logo-container' >
            <Avatar src={icon} size='larger'/>
            <Typography.Title level={2} className="logo" >
                <Link to="/">Cryptoverse</Link>
            </Typography.Title>
        </div>
        <Menu them="dark">
            <Menu.Item icon={<HomeOutlined/>}>
                <Link to="/">Home</Link>
            </Menu.Item>
            <Menu.Item icon={<FundOutlined/>}>
                <Link to="/cryptocurrencies">CryptoCurrencies</Link>
            </Menu.Item>
            <Menu.Item icon={<MoneyCollectFilled/>}>
                <Link to="/exchanges">Exchanges</Link>
            </Menu.Item>
            <Menu.Item icon={<BulbFilled/>}>
                <Link to="/news">News</Link>
            </Menu.Item>
        </Menu>
    </div>
  )
}

export default Navbar